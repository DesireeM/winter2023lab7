public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		tictactoeBoard = new Square[3][3] { { BLANK, BLANK, BLANK } {BLANK, BLANK, BLANK} {BLANK, BLANK, BLANK} };
	}
	
	public String toString()
	{
		String boardBlueprint = "";
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++) 
			{
				boardBlueprint += "\n";
            }
		}
		return boardBlueprint;
	}
	
	public boolean placeToken (int row, int col, Square playerToken)
	{
		if (row < 0 || row > 2 || col < 0 || col > 2)
		{
			return false;
		}
		
		if (tictactoeBoard[row][col] == Square.BlANK)
		{
			tictactoeBoard[row][col] = playerToken;
		}
		
		return false;
	}
	
	public boolean checkIfFull()
	{
		for(int row = 0; row < 3; row++)
		{
			for int col = 0; col < 3; col++)
			{
				if (tictactoeBoard[row][col] == Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal (Square playerToken)
	{
		for(int row = 0; row < 3 ; row++)
		{
			if (tictactoeBoard[row][0] == playerToken && tictactoeBoard[row][1] == playerToken && tictactoeBoard[row][2] == playerToken)
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical (Square playerToken)
	{
		for(int col = 0; col < 3 ; col++)
		{
			if (tictactoeBoard[0][col] == playerToken && tictactoeBoard[1][col] == playerToken && tictactoeBoard[2][col] == playerToken)
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning (Square playerToken)
	{
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(Square playerToken))
		{
			return true;
		}
		return false;
	}
	